#qpy:2
#qpy:console
"""
@author: Elias Wood (owns13927@yahoo.com)
2015-12-03
mydb - database abstraction
Most copied from pymygamedb.mydb
"""

import os
import sqlite3 as sql
from threading import Lock as Thread_Lock
from myloggingbase import MyLoggingBase

# for downloading source files...
import csv # for import/export
import logging # to stop requests from logging
from fileinput import filename
logging.getLogger('requests').setLevel(logging.WARNING)

ADD_ID = 0
ADD_TAX_YR = 1
ADD_CERT_NM = 2
ADD_TOWNSHIP = 3
ADD_PARCEL_ID = 4
ADD_ADDRESS = 5
ADD_CODE = 6
ADD_SALE_DT = 7
ADD_ESTIMATE = 8
ADD_VIEWED = 9
ADD_FAV = 10
ADD_CREATED_DT = 11
ADD_UPDATED_DT = 12

class MyDb(MyLoggingBase):
    
    __filename = None
    __db = None
    __lock = None
    
    def __init__(self,*args,**keys):
        """filename: dbname"""
        if len(args) > 0:
            filename = args[0]
            args = args[1:]
        else:
            filename = keys.pop('filename',None)
        
        MyLoggingBase.__init__(self,*args,**keys)
        
        self.__lock = Thread_Lock()
        
        if filename: self.open(filename)
        
    #===========================================================================
    # Create a new Db
    #===========================================================================
    def new(self,filename):
        """create a new db.  Will NOT overwrite an existing file! returns """
        with self.__lock:
            # close current db if one is open
            if self._is_open(): self._close()
            # open db
            db = self._try_open(filename)
            if db == None: return False
            # success
            self.__filename = filename
            self.__db = db
            
            self.logger.info('database created "%s"',filename)
            return self._init_db_structure()
        
    def _init_db_structure(self):
        # 'override'...
        if not self.__init_db_structure(): return False
        else:
            self.logger.info('inserting initial records! please wait...')
            # laod data...
            data_file = get_resource('data.csv')
            with open(data_file,'rb') as r,self.__db as conn:
                c = csv.reader(r,delimiter=',',quotechar='"',
                               doublequote=True,skipinitialspace=True)
                c.next() # skip headers!
                
                added = 0
                already_added = 0
                success = True
                for row in c:
                    # insert to DB!
                    try: conn.execute('''
                                INSERT INTO adds (
                                    tax_yr,
                                    cert_nm,
                                    township,
                                    parcel_id,
                                    address,
                                    code,
                                    sale_dt,
                                    estimate
                                ) VALUES (?,?,?,?,?,?,?,?)''',
                                (row[0],
                                 row[1],
                                 row[2],
                                 row[3],
                                 row[4],
                                 row[5],
                                 row[6],
                                 row[7])
                        )
                    except sql.IntegrityError, e: already_added += 1
                    except sql.Error, e:
                        self.logger.critical('failed. %s: %r. row=%s',
                                             e.__class__.__name__,e,
                                             row)
                        success = False
                        break
                    else: added += 1
            
            # report what happened
            self.logger.debug('%d lines inserted',added)
            self.logger.debug('%d lines already inserted ("cert_nm" or "parcel_id" col)',
                              already_added)
            
            # handle failure!
            if success: pass
            else: pass
            
            return True
        
        
    def __init_db_structure(self):
        """used internally to init structure"""
        initSQL = get_resource('initdb.sql')
        
        if os.path.exists(initSQL):
            with open(initSQL,'r') as r:
                s = r.read()
            return self._run_ddl(s)    
        else:
            self.logger.critical('the DDL (%s) for initializing the db DNE!',
                                 initSQL)
            return False
        
    def run_ddl(self,s):
        with self.__lock:
            if self._is_open():
                return self._run_ddl(s)
            else:
                self.logger.warn('the db needs to be open before running a ddl!')
                return False
            
    def _run_ddl(self,s):
        # s is the DDL text!
        self.logger.debug('running ddl """\n%s"""',s)
        try:
            self.__db.executescript(s)
        except sql.Error, e:
            # log and return failure!
            self.logger.critical('failed. %s: %r',e.__class__.__name__,e)
            return False
        else: return True
    
    #===========================================================================
    #=============================== DB FILE IO ================================
    #===========================================================================
    def _try_open(self,filename):
        """close prev db and try to open the new one. NOTE: will create."""
        db = None
        try: db = sql.connect(filename,
                              check_same_thread=False,
                              isolation_level=None) # auto-commit
        except sql.Error, e:
            # log and return failure!
            self.logger.critical('failed. %s: %r',e.__class__.__name__,e)
            return None
        # everything went well
        else: return db
    
    def open(self,filename,create=True):
        """If create is True, a new db will be created if the specified one DNE.
        # NOTE: pass ':memory:' for filename to get an 'in memory' db"""
        
        # open db
        if os.path.exists(filename):
            with self.__lock:
                db = self._try_open(filename)
                if db==None: return False
                else:
                    # close current db if one is open
                    if self._is_open(): self._close()
                    # save new db
                    self.__db = db
                    self.__filename = filename
            self.logger.info('database opened - "%s"',filename)
            return True
        
        # create an in memory db
        elif filename==':memory:': return self.new(filename)
        
        # create if requested (can use new instead...)
        elif create:
            # make sure the file can be created
            try: open(filename,'w').close()
            except IOError as e:
                # log and return failure!
                self.logger.warn('IOError with filename %s. %s: %r',
                                 filename,e.__class__.__name__,e)
                return False
            else:
                os.remove(filename) # delete the file!
                return self.new(filename)
        else:
            self.logger.warn('file DNE and you requested not to create one - no action taken!')
            return False
    
    #===========================================================================
    # Close the current db
    #===========================================================================
    def close(self,commit=True):
        """close db"""
        with self.__lock:
            if self._is_open():
                return self._close(commit)
    
    def _close(self,commit=True):
        # try to commit - though shouldn't be needed
        try:
            if commit: self.__db.commit()
            self.__db.close()
        except sql.Error, e:
            self.logger.critical('failed. %s: %r',e.__class__.__name__,e)
            return False
        else:
            self.__db = None
            return True
        
    #===========================================================================
    #============================ Getters / Setters ============================
    #===========================================================================
    def is_open(self):
        with self.__lock:
            is_open = self._is_open()
        return is_open
    
    def _is_open(self):
        return self.__db != None
    
    def get_db(self):
        return self.__db
    
    def get_filename(self):
        return self.__filename
    
    def get_lock(self):
        return self.__lock
    
    def get_tables(self):
        """returns the list of tables or None if something failed..."""
        retList = []
        with self.__lock:
            if self._is_open():
                retList = self._get_tables()
        return retList
    
    def _get_tables(self):
        try:
            with self.__db as conn:
                return conn.execute("SELECT name FROM sqlite_master WHERE type='table'").fetchall()
        except sql.Error as e:
            self.logger.warning('failed!?!? %r',e)
        return []
    
    
    
    ''
    #===========================================================================
    #====================== interaction methods ================================
    #===========================================================================
    def get_next(self,*args,**keys):
        with self.__lock:
            if self._is_open():
                next_v = self._get_next(*args,**keys)
            else:
                self.logger.warn('the db needs to be open!')
                next_v = None
        return next_v
    
    def _get_next(self,viewed=None,favorite=None,prev_item_id=None):
        """
        """
        next_v = None
        with self.__db as conn:
            # update previous is provided
            if prev_item_id!=None:
                try: conn.execute('''UPDATE adds
                    SET updated_dt = CURRENT_TIMESTAMP, viewed = 1
                    WHERE id=?''',(prev_item_id,))
                except sql.Error, e:
                    self.logger.critical('prev_id viewed=1 failed. %s: %r.',
                                         e.__class__.__name__,e)
            
            # get next
            try: next_v = conn.execute('''
                    SELECT *
                    FROM adds
                    WHERE '''+
                    ('' if viewed==None else 'viewed=:viewed and ')+
                    ('' if favorite==None else 'fav=:fav and ')+
                    '''1=1
                    ORDER BY id
                    LIMIT 1
                ''',{'viewed':1 if viewed else 0,
                     'fav':1 if favorite else 0}).fetchone()
            except sql.Error, e:
                self.logger.critical('failed. %s: %r.',
                                     e.__class__.__name__,e)
                next_v = None
        return next_v
    
    def mark_flag(self,*args,**keys):
        with self.__lock:
            if self._is_open():
                success = self._mark_flag(*args,**keys)
            else:
                self.logger.warn('the db needs to be open!')
                success = False
        return success
    
    def _mark_flag(self,item_id,key='viewed',val=True):
        # key = (viewed | fav)
        success = True
        with self.__db as conn:
            try: conn.execute('''UPDATE adds
                    SET updated_dt = CURRENT_TIME,'''+
                    ('fav' if key=='fav' else 'viewed')+'''=?
                    WHERE id=?
                    ''',(1 if val else 0,item_id)).fetchall()
            except sql.Error, e:
                self.logger.critical('failed. %s: %r.',
                                     e.__class__.__name__,e)
                success = False
        return success
    ''
    #===========================================================================
    #============================ for fun ======================================
    #===========================================================================
    def enter_interactive_mode(self):
        from pprint import pprint
        with self.__lock:
            if self._is_open():
                print 'entered interactive mode. start typing SQL!'
                print 'enter \q to exit/quit'
                print 'enter \c to run python cmds (one-liners)'
                buff = ''
                line = ''
                with self.__db as conn:
                    while True:
                        line = raw_input('>')
                        if line == r'\q':
                            print 'exiting...'
                            break
                        elif line.startswith(r'\c'):
                            print 'executing "%s"' % (line[2:].strip(),)
                            try: exec(line[2:].strip())
                            except Exception, e:
                                self.logger.critical('failed. %s: %r',
                                                     e.__class__.__name__,e)
                        else:
                            buff += ' ' + line
                            if sql.complete_statement(buff):
                                try:
                                    a = conn.execute(buff.strip())
                                    
                                    if buff.lstrip()[:6].upper() == 'SELECT':
                                        pprint(a.fetchall())
                                
                                except sql.Error, e:
                                    self.logger.critical('failed. %s: %r',
                                                         e.__class__.__name__,
                                                         e)
                                finally: buff = ''
            else:
                self.logger.warn('cannot open interactive mode on a closed db')
                return False
    
def get_resource(s):
    return os.path.realpath(os.path.join(os.path.dirname(__file__),
                                         'resources',s))

if __name__ == '__main__':
    MyLoggingBase.init_logging(file_log_lvl=None)
    
    mydb = MyDb(':memory:')
    mydb.enter_interactive_mode()
    
    
    
    
    
    
    
    