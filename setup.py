# -*- coding: utf-8 -*-

import sys
from cx_Freeze import setup, Executable

options = {
    'build_exe': {
        'compressed': True,
        'includes': [
            'myloggingbase',
            'mydb'
        ],
        'path': sys.path + ['']
    }
}

base = None
if sys.platform == 'win32':
    base = 'Win32GUI'

executables = [
    Executable('main.py')#, base=base)
]

setup(name='pyaddresses',
      version='0.1',
      description='application to go through addresses',
      executables=executables
      )
