"""
Elias Wood
2015-12-03
simple script to take SCCASN.txt and make into nice CSV
"""
import csv
import re

input_file = 'SCCASN.txt'
output_file = 'data.csv'
#"""
with open(input_file,'rb') as r:
    with open(output_file,'wb') as w: 
        c = csv.writer(w,delimiter=',',quotechar='"',
                       doublequote=True,skipinitialspace=True)
        
        # write headers
        c.writerow(('Tax Year','Certificate #','Township',
                    'Parcel I.D. #','Property Address','Use Code',
                    'Tax Sale Date','Estimated Amount'))
        
        # track things
        line_cnt = 0
        add_cnt = 0
        
        cont_match = False
        cont_nm = 0
        cont_cnt = 0
        
        # init regex to break up the line
        p = re.compile(r'''^
                \+(?P<year>\d{4})\s+            # Tax Year
                (?P<cert>\d+)\s+                # Certificate #
                (?P<ts>(\w+(\s(?!\s))?)+)\s+    # Township
                (?P<pid>[\d\.-]+)\s+            # Parcel I.D. #
                (?P<add>(\S+(\s(?!\s))?)+)\s+   # Property Address
                (?P<code>\d+)?\s+               # Use Code
                (?P<td>[\d/]+)\s+               # Tax Sale Date
                (?P<ea>.+$)                     # Estimated Amount
            ''',re.VERBOSE|re.IGNORECASE)
        
        # for each line in the file....
        for l in (s.rstrip() for s in r.readlines()):
            line_cnt += 1
            
            m = p.match(l)
            
            # write if we have a match!
            if m:
                add_cnt += 1
                c.writerow(m.group('year','cert','ts','pid',
                                     'add','code','td','ea'))
                
                # first one in this batch
                if not cont_match:
                    cont_match = True
                    cont_nm += 1
                    print 'PAGE {:02} has {:02} records'.format(
                                cont_nm,cont_cnt)
                    cont_cnt = 0
                
                # we found an address!!!
                cont_cnt += 1
                
            else: cont_match = False
            
                
print '{} lines {} addresses'.format(line_cnt,add_cnt)

"""
# check for unique
with open(output_file,'rb') as r:
    c = csv.reader(r,delimiter=',',quotechar='"',
                   doublequote=True,skipinitialspace=True)
    
    a = set()
    b = set()
    lines = 0
    for l in c:
        lines += 1
        if l[1] in a: print '1 is not unique!'
        else: a.add(l[1])
            
        if l[3] in b: print '3 is not unique!'
        else: b.add(l[3])
    
print 'lines:',lines
#"""