-- all the source we download from!
CREATE TABLE adds (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    tax_yr TEXT NOT NULL,
    cert_nm TEXT UNIQUE NOT NULL,
    township TEXT NOT NULL,
    parcel_id TEXT UNIQUE NOT NULL,
    address TEXT NOT NULL,
    code TEXT,
    sale_dt DATE NOT NULL,
    estimate REAL NOT NULL,
    viewed INTEGER NOT NULL DEFAULT 0,
    fav INTEGER NOT NULL DEFAULT 0,
    created_dt  TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_dt  TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);
