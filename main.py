'''
Created on Dec 3, 2015

@author: owns
'''
from os import startfile
from pprint import pprint

from myloggingbase import MyLoggingBase
import mydb

VIEW_NORMAL = 0
VIEW_RAW = 1

class MyConsoleInterface(MyLoggingBase):
    cur = None
    __db = None
    viewed = False
    fav = None
    auto_open = True
    view_mode = VIEW_NORMAL
    state = 'IL'
    
    _CMDS = (('fav/unfav','add/remove the item to your favorites'),
             ('nfav','add the item to your favorites and move to the next'),
             ('next|n','go to the next address (mark current has viewed)'),
             ('show','show the current open again'),
             ('help','show all commands'))
    
    _SCMDS = (('mode (normal|raw)','change to show normal or all details'),
              ('open','open the current item in google maps'),
              ('auto open (on|off)','auto open in google'),
              ('change view <viewed> <favorite>','change current list mode'),
              ('debug <command>','execute python commands'),
              ('sql','enter sql mode'))
    
    def __init__(self,db,*args,**keys):
        # db must be open and ready to go!!!!
        MyLoggingBase.__init__(self,*args,**keys)
        self.set_db(db)
        
    def get_db(self):
        return self.__db
    
    def set_db(self,db):
        self.__db = db    
    
    def stop(self):
        self.logger.info('exiting...')
        print 'good-bye'
        self.__db.close()
        
    def start(self):
        print 'hello! this is a console application! useful shotcut-keys...'
        print '<ALT>+<TAB> - switch between windows'
        print "<CTRL>+W    - close the current brower'tab"
        print "<ALT>+<F4>  - close the current application"
        
        print 'if you have any questions/comments/concerns, email:'
        print '\towns13927@yahoo.com'
        print 'ENJOY!'
        
        self.run()
    
    def open_address(self,cur):
        if cur != None:
            startfile('https://www.google.com/maps/place/%s, %s %s' % 
                    (cur[mydb.ADD_ADDRESS].replace(' ','+'),
                     cur[mydb.ADD_TOWNSHIP].replace(' ','+'),self.state))
    
    def main_interface(self,cur,view_all_cmds=False):
        # open current in google
        if self.auto_open: self.open_address(cur)
        # output current sel
        print '================================================================'
        print '========= VIEWED={} FAVORITE={} ========='.format(
                                                        self.viewed,self.fav)
        if cur:
            if self.view_mode == VIEW_NORMAL:
                print 'ADDRESS: {}, {} {}'.format(cur[mydb.ADD_ADDRESS],
                                                  cur[mydb.ADD_TOWNSHIP],
                                                  self.state)
                print 'VIEWED:{} FAVORITE:{}'.format(cur[mydb.ADD_VIEWED],
                                                     cur[mydb.ADD_FAV])
            else: pprint(cur)
        else: print 'None Left!'
            
        # output help
        print '========= COMMANDS ========='
        for cmd,info in (self._CMDS + (self._SCMDS if view_all_cmds
                                       else tuple())
                         ):
            print '{} - {}'.format(cmd,info)
        print '================================================================'
    
    def run(self):
        line = ''
        move_next = False
        view_all_cmds = False
        cur = None
        while True:
            if move_next or cur==None:
                move_next = False
                cur = self.__db.get_next(viewed=self.viewed,
                                         favorite=self.fav,
                                         prev_item_id=cur[0] if cur else None)
                
                self.main_interface(cur,view_all_cmds)
            elif view_all_cmds:
                self.main_interface(cur,view_all_cmds)
                
            line = raw_input('cmd:')
            self.logger.debug('cmd: "%s"',line)
            view_all_cmds = False
            
            # exit!
            if line in (r'\q','exit','quit'):
                self.stop()
                break
            # direct cmd
            elif line.startswith('debug'):
                line = line[5:].strip()
                self.logger.debug('exec("%s")',line)
                exec(line)
            # enter sql
            elif line.startswith('sql'):
                self.__db.enter_interactive_mode()
            
            # special command
            elif line == 'open': self.open_address(cur)
            elif line.startswith('mode '):
                if line[5:] == 'normal':
                    self.view_mode = VIEW_NORMAL
                    print 'view mode changed to NORMAL'
                elif line[5:] == 'raw':
                    self.view_mode = VIEW_RAW
                    print 'view mode changed to RAW'
                else:
                    print 'view mode not recognized -',line[5:]
            elif line.startswith('auto open '):
                if line[10:] == 'on':
                    self.auto_open = True
                    print 'auto open is now on'
                elif line[10:] == 'off':
                    self.auto_open = False
                    print 'auto open is now off'
                else:
                    print 'auto open mode not recognized -',line[5:]
            elif line.startswith('change view '):
                view,fav = (line[12:].split(' ') if ' ' in line[12:]
                                     else (str(self.viewed),str(self.fav)))
                
                if view == 'None': self.viewed = None
                else: self.viewed = (True if view.lower() in ('1','true')
                                     else False)
                if fav == 'None': self.fav = None
                else: self.fav = (True if fav.lower() in ('1','true')
                                  else False)
                
                print 'view changed to viewed=%s fav=%s' % (self.viewed,self.fav)
                self.logger.debug('view changed to viewed=%s fav=%s',
                                  self.viewed,self.fav)
                
                cur = None
                move_next = True
                
            # normal cmd
            elif line == 'show':
                cur = None
                move_next = True
            elif line in ('next','n'): move_next = True
            elif line in ('fav','unfav','nfav'):
                if cur:
                    self.__db.mark_flag(cur[0],'fav',
                                        False if line=='unfav' else True)
                    print '{} favorites'.format('removed from' if line=='unfav'
                                                else 'added to')
                    if line=='nfav': move_next = True
                else:
                    print 'there must be a current item selected to mark!'
            elif line=='help': view_all_cmds = True
            else:
                self.logger.debug('unknown command "%s" - no action taken',line)
                print 'unknown command "%s" - no action taken' % (line,)
''
#===============================================================================
#=============================== MAIN ========================================== 
#===============================================================================
if __name__ == '__main__':
    MyLoggingBase.init_logging(file_log_lvl='DEBUG',console_log_lvl='INFO')
    
    db = mydb.MyDb('main.db') #mydb.get_resource('main.db'))
    #db.enter_interactive_mode()
    
    a = MyConsoleInterface(db)
    
    a.start()
    